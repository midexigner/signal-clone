import * as React from 'react';
import { StyleSheet,Text, View,FlatList} from 'react-native';
import ChatRoomItem from "../components/ChatRoomItem"

import chatRoomsData from '../assets/data/ChatRooms'


export default function TabOneScreen() {
  return (
    <View style={styles.page}>
    <FlatList 
        data={chatRoomsData}
        renderItem={({ item }) => <ChatRoomItem chatRoom={item} />}
        keyExtractor={item => item.id}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    backgroundColor: 'white',
    flex: 1
  },
});
